#!/bin/sh
# $Id: install_fortunes.sh 528 2010-03-11 16:19:34Z robert $
# Robert Luberda <robert@pingu.ii.uj.edu.pl>, 2000, GPL
#

set -e 

if [ $# -ne 1 ] ; then
	echo "Usage: $0 install_dir" 1>&2
	exit 1
fi

install_dir=$1

if [ ! -d debian ] ; then
	echo "No debian directory found" 1>&2
	exit 1
fi


if [ ! -d $install_dir ] ; then
	echo "Directory $install_dir does not exist" 1>&2
	exit 1
fi

if [ -z $STRFILE ] ; then
	STRFILE="/usr/bin/strfile"
fi

umask 022

for file in *; do
  if test -f $file  && grep -q '^%$' "$file"; then
	echo "===> Processing: \"$file\""

	# be sure, there is % and newline at the end of file
	(cat "$file"; echo; echo '%') | \
  	awk 'BEGIN  {   
       	                RS="[\n ]*%( *\n)+" 
                        ORS="\n%\n" 
                        OFS="" 
                     }
         NR <= 1     {  # in case there was no % at the beginning of file 	
		  	gsub("^([ \t]*\n)+", "")	
  		     }
         /[A-Za-z0-9]/ { 
		  	gsub("[ \t]+\n", "\n")	
		  	print $0
               	      } ' > "$install_dir/$file"

	$STRFILE "$install_dir/$file"
	chmod 644 "$install_dir/$file"
	touch -r "$file" "$install_dir/$file"
	touch "$install_dir/$file.u8"

  else 
	echo "===> Skipping: \"$file\""
  fi
  echo
done

         


